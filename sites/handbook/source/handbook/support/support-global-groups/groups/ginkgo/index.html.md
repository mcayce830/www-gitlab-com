---
layout: handbook-page-toc
title: Home Page for Support's Ginkgo Group
description: Home Page for Support's Ginkgo Group
---

<!-- Search for all occurrences of NAME and replace them with the group's name.
     Search for all occurrences of URL HERE and replace them with the appropriate url -->

# Welcome to the home page of the Ginkgo group

![a triumphant tanuki holding bread over its head under a ginkgo tree](/handbook/support/support-global-groups/groups/ginkgo/sgg_ginkgo.png)

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Ginkgo resources

- Our Slack Channel: [#spt_gg_ginkgo](https://gitlab.slack.com/archives/C0354N9B14G)
  - As a member of the Ginkgo group, consider [changing your notifications settings](#slack-notifications) to allow for better collaboration.
- Our Team: [Ginkgo Members](https://gitlab-com.gitlab.io/support/team/sgg.html?search=ginkgo)

## Ginkgo workflows and processes

### Ticket workflow

#### FRT Stage

Aim to grab a ticket from the FRT stage and become the DRI for that ticket. If you do not have capacity to take on assignment, or if the customer would benefit from having the ticket handled by another region, send out an FRT and ask in the group channel if another group member can become DRI – or at least add themselves to the CCs and help assume collective responsibility for moving the ticket forward.

#### NRT Stage

There are several variables that can be taken into consideration when deciding how to get involved with tickets in the NRT stage. As a general rule, we default to [Collaboration](https://about.gitlab.com/handbook/values/#collaboration) and all its operating principles. 

For documentation purposes, the following table was devised as a reference guideline:

```
| Breached | Assignee           | Action                                                                                                                 |
|----------|--------------------|------------------------------------------------------------------------------------------------------------------------|
| No       | has an assignee    | No action needed, any ideas can (and should be) posted as an internal or field note                                    |
| No       | no assignee or OOO | You are strongly encouraged to take it, or at least contribute with a public reply, or ideas in the form of notes      |
| Yes      | has an assignee    | Feel free to contribute however you see fit, having in mind the ongoing discussion (we don't want to repeat ourselves) |
| Yes      | no assignee or OOO | You are strongly encouraged to take assignment, or at least reply to the customer to avoid further breach	         | 

```

### Daily Pairing Sessions

 - EMEA/AMER friendly pairing sessions are held every weekday from 14:30 to 15:30 UTC.
 - AMER friendly pairing sessions are held every weekday from 18:00 to 19:00 UTC.

**Goal**: Meet as a group and work toward our [Definition of Success](../index.html.md#success-of-their-group-means).

**Guidelines**: Decide if a Pairing Session or a Crush Session is the best format for any given day, based on how the FRT queue looks and on current workload of the participants.

**Location**: These meetings will be held in the Gingko Collab Room, which can be found in the bookmarks of Our Slack Channel, [#spt_gg_ginkgo](https://gitlab.slack.com/archives/C0354N9B14G).

### Team Building Question Days

To get to know each other better we infrequently post questions that are fun but controversial in a light-hearted way on our Slack channel. Passionate participation is highly recommended, and stealing of both the process and questions by other SGGs is definitely encouraged.

Some questions we discussed so far:
 
 - In what order do you put on your shoes and socks – AA BB, or AB AB?
 - Do you put the toilet paper on the roll so that the paper comes from over the roll or under the roll?
 - Nutella – with or without butter underneath?

### Slack notifications

To update your notification settings on Slack:
1. In Slack, right click on our channel [#spt_gg_ginkgo](https://gitlab.slack.com/archives/C0354N9B14G)
2. Choose **Change notifications**
3. Change **Send a notification for** to **All new messages**
4. Save Changes

